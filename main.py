import pyodbc
import mysql.connector

from connection_pass import CONN_STRING_MSSQL, CONN_STRING_MYSQL
from sql import get_clients_info, insert_to_pbx, get_clients_info_lynx, delete_from_pbx


class DatabaseConnection(object):
    def __init__(self, connection_string):
        self.conn = pyodbc.connect(connection_string)
        self.conn.autocommit = True
        self.cursor = self.conn.cursor()

    def __enter__(self):
        return self.cursor

    def __exit__(self, *args):
        self.cursor.close()
        self.conn.close()


def get_users():
    with DatabaseConnection(CONN_STRING_MSSQL) as cursor:
        results = cursor.execute(get_clients_info)
        results_fetch = results.fetchall()

    result_out = []

    for result in results_fetch:
        user_phones = {
            'name': result.name,
            'phone': str(result.phone).strip(),
            'telefon2': str(result.Telefon2).strip(),
            'telefon3': str(result.Telefon3).strip(),
            'telefon4': str(result.Telefon4).strip(),
        }

        if user_phones['name'] and (user_phones['phone'] or user_phones['telefon2']
                                    or user_phones['telefon3'] or user_phones['telefon4']):
            if user_phones['phone']:
                cp = clear_phones(user_phones['name'], user_phones['phone'])
                if cp:
                    result_out.append([cp[0][0], cp[0][1]])
                    result_out.append([cp[1][0], cp[1][1]])

            if user_phones['telefon2']:
                cp = clear_phones(user_phones['name'], user_phones['telefon2'])
                if cp:
                    result_out.append([cp[0][0], cp[0][1]])
                    result_out.append([cp[1][0], cp[1][1]])

            if user_phones['telefon3']:
                cp = clear_phones(user_phones['name'], user_phones['telefon3'])
                if cp:
                    result_out.append([cp[0][0], cp[0][1]])
                    result_out.append([cp[1][0], cp[1][1]])

            if user_phones['telefon4']:
                cp = clear_phones(user_phones['name'], user_phones['telefon4'])
                if cp:
                    result_out.append([cp[0][0], cp[0][1]])
                    result_out.append([cp[1][0], cp[1][1]])

    with DatabaseConnection(CONN_STRING_MSSQL) as cursor:
        results = cursor.execute(get_clients_info_lynx)
        results_fetch = results.fetchall()

    for r in results_fetch:
        cp = clear_phones(r[0], r[1])
        if cp:
            result_out.append([cp[0][0], cp[0][1]])
            result_out.append([cp[1][0], cp[1][1]])

    cnx = mysql.connector.connect(**CONN_STRING_MYSQL)
    mysql_cursor = cnx.cursor()
    mysql_cursor.execute(delete_from_pbx)
    cnx.commit()

    for r in result_out:
        mysql_cursor.execute(insert_to_pbx.format(r[0], r[1]))

    cnx.commit()
    cnx.close()


def clear_phones(name, phone):
    if phone and phone != '':
        phone = str(phone).strip().replace(" ", "").replace("-", "") \
            .replace("+7", "8").replace("+8", "8").replace("(", "").replace(")", "")

        if phone[:2] == '79':
            phone = "".join('89' + phone[2:])

        if len(phone) == 7:
            phone = "".join('8863' + phone)

        if len(phone) >= 11:
            phone = phone.split(',')
            phone = phone[0]

        if len(phone) >= 11:
            phone = phone.split(';')
            phone = phone[0]

        if len(phone) >= 11:
            phone = phone.split('д')
            phone = phone[0]

        if len(phone) >= 11:
            phone = phone.split('т')
            phone = phone[0]

        if len(phone) >= 11:
            phone = phone.split('/')
            phone = phone[0]

        if len(phone) == 11:
            phone = phone[1:]

        if len(phone) > 11:
            return False

        if len(phone) < 10:
            return False

        if phone == 'None':
            return False

        r1 = [name, phone]
        r2 = [name, "".join("+7" + phone)]
        return r1, r2
    return False


if __name__ == '__main__':
    get_users()
